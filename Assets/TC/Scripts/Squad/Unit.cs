﻿using UnityEngine.AI;
using UnityEngine;
using System;
using UnityEditor;
using System.Collections.Generic;

public class Unit : MonoBehaviour
{
    [SerializeField] private UnitMovement movementController;
    [SerializeField] private UnitAnimation animationsController;
    [SerializeField] private Transform formationPosition;
    [SerializeField] private UnitState unitState;
    [SerializeField] private UnitStance unitStance;
    [SerializeField] private int teamId;
    [SerializeField] private GameObject lockedEnemy;
    [SerializeField] private int squadAttacked;
    [Header("Customization Objects")]
    [SerializeField] private Transform helmetObj;
    [SerializeField] private Transform primaryWeapon;
    [SerializeField] private Transform offHand;
    [SerializeField] private SkinnedMeshRenderer meshRenderer;
    public string squadId;
    private bool followingEnemyInCombat = false;
    private bool canMove = false;
    // public float attackCooldown = 1.5f;
    private int hp;
    private int attack; //How many attacks per minute
    private int defense; //Chance of blocking attack
    private int charge; //Charge Damage
    private int speed; //Movement speed
    private int morale; //Morale meter
    private int stamina; //Depletes every time you run/attack/defend
    private int range; //Range of visibility
    private bool isCommander = false;
    private string preferredName = "";

    public enum UnitStance
    {
        aggressive,
        defensive,
        patrol,
        neutral
    }

    public enum UnitState
    {
        idle,
        combat,
        route,
        march,
        run,
        death
    };

    void Start()
    {

    }

    #region PROPERTIES

    public UnitState currentState
    {
        get { return unitState; }
        set { unitState = value; }
    }

    public UnitStance currentStance
    {
        get { return unitStance; }
        set { currentStance = value; }
    }

    public int TeamId
    {
        get { return teamId; }
        set { teamId = value; }
    }

    public GameObject LockedEnemy
    {
        get { return lockedEnemy; }
        set { lockedEnemy = value; }
    }
    #endregion //PROPERTIES

    public void SetupUnit(UnitDetails ud, bool showcase = false, Transform formationPos = null, string squad = "0", int team = 0, UnitStance st = UnitStance.neutral)
    {
        preferredName = ud.preferredName;
        if (!showcase)
        {
            formationPosition = formationPos;
            movementController.Warp(formationPos.position);
            squadId = squad;
            teamId = team;
            unitState = UnitState.idle;
            unitStance = st;
            attack = ud.attack;
            defense = ud.defense;
            charge = ud.charge;
            speed = ud.speed;
            morale = ud.morale;
            range = ud.range;
        }
        isCommander = ud.isCommander;
        // Animator a = HelperFiles.instance.GetUnitAnimator(ud.animator);
        // animationsController.SetAnimationController(a.runtimeAnimatorController);
        SetupWeaponsAndArmor(ud);

    }

    private void SetupWeaponsAndArmor(UnitDetails ud)
    {
        /*
        Step 0: choose color
        Step 1: choose facial hair
        Step 2: choose facial scar
        Step 3: Choose Helmet
        Step 4: Choose Plume
        Step 5: choose additional item
        */
        Appearance armorSettings = ud.appearance.Find(x => x.name.Equals("armor"));
        Appearance colorSettings = ud.appearance.Find(x => x.name.Equals("tunic"));
        Appearance facesSettings = ud.appearance.Find(x => x.name.Equals("faces"));
        Appearance beardsSettings = ud.appearance.Find(x => x.name.Equals("beards"));
        foreach (Material m in meshRenderer.materials)
        {
            if (m.name.Equals("skin (Instance)"))
            {
                m.SetTextureOffset("_BaseMap", new Vector2(ud.skinToneOffsetX, 0));
            }
            if (m.name.Equals("armor (Instance)"))
            {
                m.SetTextureOffset("_BaseMap", new Vector2(armorSettings.offsetX, armorSettings.offsetY));
            }
            if (m.name.Equals("tunic (Instance)"))
            {
                m.SetTextureOffset("_BaseMap", new Vector2(colorSettings.offsetX, colorSettings.offsetY));
            }
            if (m.name.Equals("beards (Instance)"))
            {
                m.SetTextureOffset("_BaseMap", new Vector2(beardsSettings.offsetX, beardsSettings.offsetY));
            }
            if (m.name.Equals("faces (Instance)"))
            {
                m.SetTextureOffset("_BaseMap", new Vector2(facesSettings.offsetX, facesSettings.offsetY));
            }

        }

        List<Transform> helmetsList = new List<Transform>();
        foreach (Transform child in helmetObj)
        {
            if (child.parent == helmetObj)
                helmetsList.Add(child);
        }
        Transform[] helmets = helmetsList.ToArray();
        Transform[] offHands = offHand.GetComponentsInChildren<Transform>();
        Transform[] weapons = primaryWeapon.GetComponentsInChildren<Transform>();


        for (int i = 0; i < helmets.Length; i++)
        {
            if (!helmets[i].gameObject.name.Equals(ud.helmetObj))
            {
                Destroy(helmets[i].gameObject);
            }
            else
            {

                MeshRenderer helmetMaterial = helmets[i].GetComponent<MeshRenderer>();
                helmetMaterial.materials[0].SetTextureOffset("_BaseMap", new Vector2(armorSettings.offsetX, armorSettings.offsetY));
                for (int n = 0; n < helmetMaterial.materials.Length; n++)
                {
                    if (helmetMaterial.materials[n].name.Equals("armor (Instance)"))
                    {
                        helmetMaterial.materials[n].SetTextureOffset("_BaseMap", new Vector2(armorSettings.offsetX, armorSettings.offsetY));
                    }
                    if (helmetMaterial.materials[n].name.Equals("plume (Instance)"))
                    {
                        helmetMaterial.materials[n].SetTextureOffset("_BaseMap", new Vector2(0f, 0.66f));
                    }
                }

                if (isCommander)
                {
                    // PLUMES
                    List<Transform> plumesList = new List<Transform>();
                    foreach (Transform plume in helmets[i])
                    {
                        if (plume.parent == helmets[i])
                        {
                            plumesList.Add(plume);
                        }
                    }
                    Transform[] plumes = plumesList.ToArray();
                    for (int j = 0; j < plumes.Length; j++)
                    {
                        if (!plumes[j].gameObject.name.Equals(ud.plume))
                        {
                            Destroy(plumes[j].gameObject);
                        }
                        else
                        {
                            MeshRenderer plumesMaterial = plumes[j].GetComponent<MeshRenderer>();
                            for (int l = 0; l < plumesMaterial.materials.Length; l++)
                            {
                                if (plumesMaterial.materials[l].name.Equals("armor (Instance)"))
                                {
                                    plumesMaterial.materials[l].SetTextureOffset("_BaseMap", new Vector2(armorSettings.offsetX, armorSettings.offsetY));
                                }
                                if (plumesMaterial.materials[l].name.Equals("plume (Instance)"))
                                {
                                    plumesMaterial.materials[l].SetTextureOffset("_BaseMap", new Vector2(colorSettings.offsetX, colorSettings.offsetY));
                                }
                            }
                            // BLINDFOLD
                            Transform[] additional = plumes[j].GetComponentsInChildren<Transform>();
                            for (int k = 0; k < additional.Length; k++)
                            {
                                if (!additional[k].gameObject.name.Equals(plumes[j].name) && !additional[k].gameObject.name.Equals(ud.additional))
                                {
                                    Destroy(additional[k].gameObject);
                                }
                                else
                                {
                                    if (!additional[k].gameObject.name.Equals(plumes[j].name))
                                    {
                                        MeshRenderer additionalMaterial = additional[k].GetComponent<MeshRenderer>();
                                        additionalMaterial.materials[0].SetTextureOffset("_BaseMap", new Vector2(colorSettings.offsetX, colorSettings.offsetY));

                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        for (int i = 0; i < offHands.Length; i++)
        {
            if (!offHands[i].gameObject.name.Equals(offHand.name) && !offHands[i].gameObject.name.Equals(ud.offHand))
            {
                Destroy(offHands[i].gameObject);
            }
            else
            {
                if (!offHands[i].gameObject.name.Equals(offHand.name) && !offHands[i].gameObject.name.Contains("bow"))
                {
                    MeshRenderer m = offHands[i].GetComponent<MeshRenderer>();
                    m.materials[0].SetTextureOffset("_BaseMap", new Vector2(colorSettings.offsetX, colorSettings.offsetY));
                    m.materials[1].SetTexture("_MainTex", HelperFiles.instance.GetSigilTexture(ud.sigil));
                }
            }
        }

        for (int i = 0; i < weapons.Length; i++)
        {
            if (!weapons[i].gameObject.name.Equals(primaryWeapon.name) && !weapons[i].gameObject.name.Equals(ud.primaryWeapon))
            {
                Destroy(weapons[i].gameObject);
            }
            else
            {
                if (!weapons[i].gameObject.name.Equals(primaryWeapon.name) && ud.primaryWeapon.Equals("arrow"))
                {
                    weapons[i].gameObject.SetActive(false);
                }
            }
        }
    }

    // private void SetupUnitColors(UnitDetails ud)
    // {
    //     ;


    //     // meshRenderer.materials[0]//skin
    //     // meshRenderer.materials[1];//armor
    //     // meshRenderer.materials[2]
    // }

    void Update()
    {

    }

    void OnDrawGizmos()
    {
        if (lockedEnemy != null)
        {
            // Draws a blue line from this transform to the target
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, lockedEnemy.transform.position);
        }
    }

    private void UnitStop()
    {
        if (movementController.IsMoving)
        {
            if (!movementController.Agent.pathPending)
            {
                if (movementController.Agent.remainingDistance <= movementController.Agent.stoppingDistance)
                {
                    if (!movementController.Agent.hasPath || movementController.Agent.velocity.sqrMagnitude == 0f)
                    {

                        if (currentState == Unit.UnitState.combat || currentState == Unit.UnitState.run)
                        {
                            if (LockedEnemy != null) { }
                            // unitBrain.StopAndFight();
                        }
                        else
                        {
                            // unitBrain.OnUnitStopped();
                        }
                        movementController.IsMoving = false;
                    }
                }
            }
        }
    }
    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        // // ANIMATION SETTERS
        // if (unitState == UnitState.idle)
        // {
        //     anims.Halt();
        // }

        // if (unitState == UnitState.march)
        // {
        //     anims.March();
        // }
        // if (unitState == UnitState.run)
        // {
        //     if (lockedEnemy != null)
        //     {
        //         if (lockedEnemy.GetComponent<Unit>().unitState.Equals(UnitState.death))
        //         {
        //             FindClosestEnemy();
        //             return;
        //         }
        //     }
        //     anims.Run();
        // }

        // if (unitState == UnitState.combat)
        // {
        //     if (followingEnemyInCombat)
        //     {
        //         if (lockedEnemy != null)
        //         {
        //             Unit enemy = lockedEnemy.GetComponent<Unit>();
        //             if (!enemy.currentState.Equals(UnitState.death))
        //             {
        //                 MoveToLockedUnit();
        //                 anims.Run();
        //             }
        //             else
        //             {
        //                 FindClosestEnemy();
        //             }
        //         }
        //         else
        //         {
        //             FindClosestEnemy();
        //         }
        //     }

        //     if (lockedEnemy != null)
        //     {
        //         if (!lockedEnemy.GetComponent<Unit>().unitState.Equals(UnitState.death))
        //         {
        //             this.transform.LookAt(lockedEnemy.transform, Vector3.up);
        //         }
        //         else
        //         {
        //             FindClosestEnemy();
        //         }
        //     }
        //     else
        //     {
        //         FindClosestEnemy();
        //     }

        //     if (attackCooldown < 0f)
        //     {
        //         DealDamage();
        //         attackCooldown = 1.5f;
        //     }
        //     else
        //     {
        //         anims.InCombat();
        //         attackCooldown -= Time.deltaTime;
        //     }
        // }
    }


    // private void FindClosestEnemy()
    // {
    //     if (transform.gameObject.tag.Equals("EnemyUnits"))
    //     {
    //         GameObject[] nearbyEnemies = GameObject.FindGameObjectsWithTag("PlayerUnits");
    //         float distance = 50f;
    //         if (nearbyEnemies.Length != 0)
    //         {
    //             followingEnemyInCombat = true;
    //             foreach (GameObject enemy in nearbyEnemies)
    //             {
    //                 float currentDistance = Vector3.Distance(this.transform.position, enemy.transform.position);
    //                 if (currentDistance < distance)
    //                 {
    //                     distance = currentDistance;
    //                     lockedEnemy = enemy;
    //                 }
    //             }
    //             MoveToLockedUnit();
    //         }
    //         else
    //         {
    //             followingEnemyInCombat = false;
    //             MoveUnit();
    //         }
    //     }

    //     if (transform.gameObject.tag.Equals("PlayerUnits"))
    //     {
    //         GameObject[] nearbyEnemies = GameObject.FindGameObjectsWithTag("EnemyUnits");
    //         float distance = 50f;
    //         if (nearbyEnemies.Length != 0)
    //         {
    //             followingEnemyInCombat = true;
    //             foreach (GameObject enemy in nearbyEnemies)
    //             {
    //                 float currentDistance = Vector3.Distance(this.transform.position, enemy.transform.position);
    //                 if (currentDistance < distance)
    //                 {
    //                     distance = currentDistance;
    //                     lockedEnemy = enemy;
    //                 }
    //             }
    //             MoveToLockedUnit();
    //         }
    //         else
    //         {
    //             followingEnemyInCombat = false;
    //             MoveUnit();
    //         }
    //     }


    // }

    // public void MoveUnit()
    // {
    //     movement.MoveAgent(formationPosition.position);
    //     unitState = UnitState.march;
    //     // anims.March();
    // }

    // public void StopUnit()
    // {
    //     movement.StopAgent();
    //     OnUnitStopped();
    // }

    // public void AttackSquad(int squad)
    // {
    //     squadAttacked = squad;
    //     movement.ChargeAgent(formationPosition.position);
    //     unitState = UnitState.run;
    // }

    // private void MoveToLockedUnit()
    // {
    //     if (lockedEnemy != null)
    //         movement.ChargeAgent(lockedEnemy.transform.position);
    // }

    // public void EnemyLeavesRange(GameObject otherUnit)
    // {
    //     switch (this.unitState)
    //     {
    //         case UnitState.combat:
    //             // GoIdle();
    //             break;
    //     }
    // }

    // public void StopAndFight()
    // {
    //     movement.StopAndFight();
    //     unitState = UnitState.combat;

    //     // anims.Attack();
    // }

    // public void EnemyInRange(GameObject otherUnit)
    // {
    //     switch (this.unitState)
    //     {
    //         case UnitState.idle:
    //             // if (unitStance.Equals(UnitStance.aggressive))
    //             // {
    //             //     lockedEnemy = otherUnit;
    //             //     anims.Run();
    //             //     movement.ChargeEnemy(otherUnit);
    //             // }
    //             break;
    //         case UnitState.run:
    //             // lockedEnemy = otherUnit;
    //             // movement.ChargeEnemy(otherUnit);
    //             break;
    //     }
    // }

    // public void EnemyInAttackRange(GameObject otherUnit)
    // {
    //     switch (this.unitState)
    //     {
    //         case UnitState.idle:
    //             unitState = UnitState.combat;
    //             break;
    //         case UnitState.march:

    //             break;
    //         case UnitState.run:
    //             lockedEnemy = otherUnit;
    //             StopAndFight();
    //             break;
    //         default:
    //             break;
    //     }
    // }

    // public void OnUnitStopped()
    // {
    //     unitState = UnitState.idle;
    //     this.transform.rotation = formationPosition.rotation;
    //     // anims.Halt();
    // }

    // private void TakeHit()
    // {
    //     if (!unitState.Equals(UnitState.death))
    //     {
    //         hp--;
    //         if (hp <= 0)
    //         {
    //             //Im dead
    //             unitState = UnitState.death;
    //             this.gameObject.tag = "dead";
    //             // anims.Death1();
    //             DisableObject();
    //             Invoke("DisposeOfUnit", 10f);
    //         }
    //         // if (unitState.Equals(UnitState.idle))
    //         // {
    //         //     anims.TakeHit();
    //         //     unitState = UnitState.combat;
    //         // }
    //     }


    //     // else
    //     // {
    //     //     anims.TakeHit();
    //     // }
    // }
    // private void DealDamage()
    // {
    //     if (lockedEnemy != null)
    //     {
    //         // anims.Attack();
    //         Unit enemyUnitBrain = lockedEnemy.GetComponent<Unit>();
    //         BattleFieldCommander.instance.AIAlertSquad(enemyUnitBrain.squadId);
    //         // enemyUnitBrain.LockedEnemy = this.gameObject;
    //         // enemyUnitBrain.currentState = UnitState.combat;
    //         if (!enemyUnitBrain.unitState.Equals(UnitState.death))
    //         {
    //             enemyUnitBrain.TakeHit();
    //         }
    //     }
    // }



    // private void DisposeOfUnit()
    // {
    //     Destroy(this.gameObject.GetComponent<Unit>().formationPosition.gameObject);
    //     Destroy(this.gameObject);
    // }

    // private void DisableObject()
    // {
    //     this.gameObject.GetComponent<Rigidbody>().detectCollisions = false;
    //     this.gameObject.GetComponent<CapsuleCollider>().enabled = false;
    //     this.gameObject.GetComponent<NavMeshAgent>().enabled = false;
    //     movement.enabled = false;
    // }
}
