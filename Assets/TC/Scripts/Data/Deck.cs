﻿using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class Deck
{
    public string id;
    public string createdAt;
    public string name;
    public List<UnitDetails> units;
}
