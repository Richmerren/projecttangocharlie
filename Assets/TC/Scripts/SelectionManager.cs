﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class SelectionManager : MonoBehaviour
{
    public static SelectionManager instance;

    // [SerializeField] private TextAsset _availableUnits;

    // [Header("Default")]
    // [SerializeField] private List<Sprite> _unitTypeIcon;
    // [SerializeField] private GameObject _tokenPrefab;
    // [SerializeField] private Transform _tokenContentParent;
    // [SerializeField] private Transform _unitSpawnPoint;

    // private Dictionary<string, GameObject> _spawnedUnits;
    // private GameObject _currentSelected;

    // public GameObject SelectedUnit
    // {
    //     get { return _currentSelected; }
    // }

    // void Start()
    // {
    //     _spawnedUnits = new Dictionary<string, GameObject>();
    //     if (instance == null)
    //         instance = this;
    //     Core.instance.Player = JsonUtility.FromJson<PlayerDetails>(_availableUnits.text);
    //     SpawnUnitTokens();
    // }

    // // Update is called once per frame
    // void Update()
    // {

    // }

    // private void SpawnUnitTokens()
    // {
    //     foreach (UnitDetails ud in Core.instance.Player.unitInventory)
    //     {
    //         GameObject token = Instantiate(_tokenPrefab, _tokenContentParent);
    //         token.name = token.name.Replace("(Clone)", "");
    //         UnitTokenDisplay utd = token.GetComponent<UnitTokenDisplay>();
    //         utd.SetupToken(ud);
            
    //         GameObject unit = Instantiate(Resources.Load(ud.isCommander ? ud.heroPrefabPath : ud.unitPrefabPath, typeof(GameObject)), _unitSpawnPoint) as GameObject;
    //         unit.name = ud.name;
    //         UnityEngine.AI.NavMeshAgent nma = unit.GetComponent<UnityEngine.AI.NavMeshAgent>();
    //         nma.enabled = false;
    //         unit.AddComponent<RotateUnitPreview>();
    //         Pawn u = unit.GetComponent<Pawn>();
    //         // u.SetupUnit(ud, true);
    //         _spawnedUnits.Add(ud._id, unit);
    //         unit.SetActive(false);
    //     }
    // }

    // public void ShowSelectedUnit(string id)
    // {
    //     if (_currentSelected != null)
    //         _currentSelected.SetActive(false);
    //     _currentSelected = _spawnedUnits[id];
    //     _currentSelected.SetActive(true);
    // }

    // public void GetUnitIcon(string _type, ref Image img)
    // {
    //     switch (_type)
    //     {
    //         case "InfantryRanged":
    //             img.sprite = _unitTypeIcon[0];
    //             break;
    //         case "InfantrySpear":
    //             img.sprite = _unitTypeIcon[1];
    //             break;
    //         case "InfantrySword":
    //             img.sprite = _unitTypeIcon[2];
    //             break;
    //         case "InfantryPike":
    //             img.sprite = _unitTypeIcon[3];
    //             break;
    //         case "CavalryRanged":
    //             img.sprite = _unitTypeIcon[4];
    //             break;
    //         case "CavalrySpear":
    //             img.sprite = _unitTypeIcon[6];
    //             break;
    //         case "CavalrySword":
    //             img.sprite = _unitTypeIcon[7];
    //             break;
    //     }
    // }
}
