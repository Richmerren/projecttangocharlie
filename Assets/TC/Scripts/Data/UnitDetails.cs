﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class UnitDetails
{
    public string _id;
    public string name;
    public string description;
    public string preferredName;
    public string unitClass;
    public bool isCommander;
    public string commanderName;
    public int width;
    public int depth;
    public float xSpacing;
    public float zSpacing;  
    public int hp;
    public int attack;
    public int defense;
    public int accuracy;
    public int charge;
    public int speed;
    public int morale;
    public int stamina;
    public int range;
    public float experience;
    public int weaponAndArmor;
    public int tier;
    public float skinToneOffsetX;
    public List<Appearance> appearance;
    public List<Formation> formations;
    public string helmetObj;
    public string additional;
    public string plume;
    public string offHand;
    public string primaryWeapon;
    public string secondaryWeapon;
    public string unitPrefabPath;
    public string heroPrefabPath;
    public string animator;
    public string sigil;
    public string faceFile;
    public string beardFile;
}
[Serializable]
public class Appearance
{
    public string name;
    public float offsetX;
    public float offsetY;
}
[Serializable]
public class Formation
{
    public string name;
    public int width;
    public int depth;
    public float xSpacing;
    public float zSpacing;
    public float xDiff;
    public float zDiff;
    public List<Modifier> modifiers;
}
[Serializable]
public class PerformanceStats
{
    public int kills;
    public int lost;
    public int battles;
    public int routed;
    public int killedGeneral;
    public int lastStanding;
    public int firstBlood;
}
[Serializable]
public class Modifier
{
    public int hp;
    public int attack;
    public int defense;
    public int charge;
    public int speed;
    public int morale;
    public int stamina;
    public int range;
}
[Serializable]
public enum UnitClass
{
    InfantrySpear,
    InfantryPike,
    InfantrySword,
    InfantryRanged,
    CavalryRanged,
    CavalrySpear,
    CavalrySword
}