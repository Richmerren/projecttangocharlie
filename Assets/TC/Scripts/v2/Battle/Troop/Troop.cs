﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Troop : MonoBehaviour
{
    public string _id;
    [SerializeField] private int _teamId;
    [SerializeField] private string _customTag;
    [SerializeField] private UnitDetails _troopData;
    [SerializeField] private GameObject _wayPointPrefab;
    [SerializeField] private TroopBanner _banner;
    [SerializeField] private float _moveDelay = 0.1f;
    private Pawn[,] troopUnits;
    private GameObject[] localPositions;
    private Appearance armor;
    private Appearance tunic;


    private bool isSelected;
    private bool hasTroop;
    private bool isCreated = false;
    public string TroopId
    {
        get { return _id; }
    }
    public UnitDetails UnitInfo
    {
        get
        {
            return _troopData;
        }

        set
        {
            _troopData = value;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetBanner(TroopBanner banner)
    {
        _banner = banner;
        _banner.troopId = _id;
        _banner.SetupBanner(_troopData);
        _banner.gameObject.SetActive(false);
    }

    public void CreateTroop(UnitDetails data, bool isPlayerUnits, GameObject unitPrefab, string id)
    {
        _id = id;
        _customTag = isPlayerUnits ? "PlayerUnits" : "EnemyUnits";
        _troopData = data;
        SpawnSoldiers(unitPrefab);
    }
    // REMINDER: Spawn all army on start of scene and object pool
    void SpawnSoldiers(GameObject _prefab)
    {        
        localPositions = new GameObject[_troopData.width * _troopData.depth];
        Vector3 squadCenter = this.transform.localPosition;
        troopUnits = new Pawn[_troopData.width, _troopData.depth];
        int wpCounter = 0;
        for (int i = 0; i < _troopData.width; i++)
        {
            for (int j = 0; j < _troopData.depth; j++)
            {
                Vector3 unitPos = PawnPositionInTroop(squadCenter, i, j, _troopData.width, _troopData.depth, _troopData.xSpacing, _troopData.zSpacing);
                GameObject wp = GameObject.Instantiate(_wayPointPrefab, this.transform);
                GameObject u = GameObject.Instantiate(_prefab, unitPos, this.transform.rotation);
                wp.name = "fw_" + _id + "_" + i + "x" + j;
                wp.transform.position = unitPos;
                u.name = "unit-" + _id + "_" + i + "x" + j;
                Pawn newUnit = u.GetComponent<Pawn>();
                u.tag = _customTag;
                SetupSoldier(_troopData, u, wp.transform);
                troopUnits[i, j] = newUnit;
                u.SetActive(false);
                wp.SetActive(false);
                localPositions[wpCounter] = wp;
                wpCounter++;
            }
        }
    }

    private Vector3 PawnPositionInTroop(Vector3 center, int i, int j, int width, int depth, float xSpacing, float zSpacing)
    {
        Vector3 diff = new Vector3((i * _troopData.xSpacing) - ((_troopData.width - 1) * _troopData.xSpacing / 2f), 0f, ((j * _troopData.zSpacing) - ((_troopData.depth - 1) * _troopData.zSpacing / 2f)) * -1f);
        Vector3 unitPos = (center + diff);
        return unitPos;
    }

    public void SetTroop(Vector3 position)
    {
        this.transform.position = position;
        _banner.SetPosition(position);
        _banner.gameObject.SetActive(true);
        Vector3 squadCenter = this.transform.localPosition;
        for (int i = 0; i < _troopData.width; i++)
        {
            for (int j = 0; j < _troopData.depth; j++)
            {
                troopUnits[i, j].PlacePawn();
                troopUnits[i, j].gameObject.SetActive(true);
            }
        }
    }

    public void SetWaypoint(Vector3 position)
    {
        Vector3 limitZ = new Vector3(position.x, transform.position.y, position.z);
        this.transform.LookAt(limitZ, transform.up);
        this.transform.position = position;
        ShowWaypoints(true);
    }

    public void RotateWaypoint(Vector3 pointToLook)
    {
        Vector3 limitZ = new Vector3(pointToLook.x, transform.position.y, pointToLook.z);
        this.transform.LookAt(limitZ, transform.up);
    }

    void ShowWaypoints(bool value)
    {
        foreach (GameObject g in localPositions)
        {
            g.SetActive(value);
        }
    }

    void EnableSoldiers()
    {
        for (int i = 0; i < _troopData.width; i++)
            for (int j = 0; j < _troopData.depth; j++)
            {
                troopUnits[i, j].enabled = true;
            }
    }

    void SetupSoldier(UnitDetails unitDetails, GameObject soldierGO, Transform formationPosition)
    {
        Pawn newUnit = soldierGO.GetComponent<Pawn>();
        newUnit.SetupPawn(unitDetails, false, formationPosition, _id);
        newUnit.tag = _customTag;
    }

    public void SelectTroop(bool value)
    {
        isSelected = value;
        _banner.SetSelected(isSelected);
    }

    public void CalculateCenterOfSquad()
    {
        int count = 0;
        Vector3 addedPositions = Vector3.zero;
        for (int i = 0; i < _troopData.width; i++)
        {
            for (int j = 0; j < _troopData.depth; j++)
            {
                if (troopUnits[i, j] != null)
                {
                    addedPositions += troopUnits[i, j].transform.position;
                    count++;
                }

            }
        }
        if (count == 0)
        {
            //Destroy Banner and squad
            if (_banner != null)
                Destroy(_banner.gameObject);
        }
        else
        {
            _banner.SetPosition(addedPositions / count);
        }
    }

    private void FixedUpdate()
    {
        CalculateCenterOfSquad();
    }

    public void MoveTroopCoroutine()
    {
        ShowWaypoints(false);
        StartCoroutine(PerformTroopMovement());
    }

    IEnumerator PerformTroopMovement()
    {
        for (int i = 0; i < _troopData.depth; i++)
        {
            for (int j = 0; j < _troopData.width; j++)
            {
                if (troopUnits[j, i] != null)
                {
                    troopUnits[j, i].MovePawn();
                }
            }
            yield return new WaitForSeconds(_moveDelay);
        }
    }
}
