using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class RandomizerData
{
    public List<string> animators;
    public List<string> prefabs;
    public ColorsAndFaces colorsAndFaces;
    public List<string> head;
    public List<Helmets> helmets;
    public List<UnitDataType> unitTypes;
    public List<string> names;
    public List<string> locations;

}

[Serializable]
public class UnitDataType
{
    public string type;
    public List<string> primaryHand;
    public List<string> offHand;

}

[Serializable]
public class Helmets
{
    public string type;
    public List<string> objects;
    public List<string> plumes;
}

[Serializable]
public class ColorsAndFaces
{
    public List<Offset> otherOffsets;
    public List<Offset> armorOffsets;
    public List<string> colors;
    public List<string> scars;
    public List<string> beards;
    public List<float> skinOffsets;
}

[Serializable]
public class Offset
{
    public float x;
    public float y;
}