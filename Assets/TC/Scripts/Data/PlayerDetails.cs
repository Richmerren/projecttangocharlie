using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class PlayerDetails
{
    public string id;
    public string createdAt;
    public string userName;
    public List<UnitDetails> unitInventory;
}
