﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Squad : MonoBehaviour
{
    // [SerializeField] private UnitDetails _squadData;
    // [SerializeField] private GameObject _unitPrefab;
    // [SerializeField] private GameObject _wayPointPrefab;
    // [SerializeField] private string _customTag;
    // [SerializeField] private int _squadId;
    // [SerializeField] private Pawn.UnitStance _stance;
    // [SerializeField] private int _teamId;
    // [SerializeField] private int[] _alliances;

    // [SerializeField] private float _moveDelay = 0.1f;

    // [SerializeField] private SquadBannerController _banner;
    // [SerializeField] private int _selectedFormation = 0;
    // private Transform position;
    // private Pawn[,] squadUnits;
    // private Vector3[,] displacement;

    // private bool isSelected;
    // private bool hasSquad;
    // private bool isCreated = false;


    // public int SquadId
    // {
    //     get { return _squadId; }
    // }

    // public UnitDetails UnitInfo
    // {
    //     get
    //     {
    //         return _squadData;
    //     }

    //     set
    //     {
    //         _squadData = value;
    //     }
    // }

    // private void Awake()
    // {

    // }

    // private void Start()
    // {
    //     // position = this.transform;
    //     // SquadData squad = new SquadData
    //     // {
    //     //     name = "Spear Infantry",
    //     //     squadId = this.squadId,
    //     //     unitClass = SquadClass.infantry,
    //     //     hp = 100f,
    //     //     attk = 5f,
    //     //     def = 2f,
    //     //     speed = 1f,
    //     //     morale = 2f,
    //     //     stamina = 3f,
    //     //     range = 0.5f,
    //     //     experience = 1,
    //     //     skinToneOffsetY = 0.145f,
    //     //     formations[_selectedFormation].width = 4,
    //     //     formations[_selectedFormation].depth = 2,
    //     //     formations[_selectedFormation].xSpacing = 1.2f,
    //     //     formations[_selectedFormation].zSpacing = 1.2f
    //     // };
    //     // unitInfo = squad;
    //     // CreateUnits();

    // }

    // public void SetupSquad(UnitDetails data)
    // {
    //     _squadData = data;
    //     CreateUnits();
    //     // BattleFieldCommander.instance.AddSquad(this);
    // }

    // private void CreateUnits()
    // {
    //     Vector3 squadCenter = position.localPosition;
    //     squadUnits = new Pawn[_squadData.formations[_selectedFormation].width, _squadData.formations[_selectedFormation].depth];
    //     for (int i = 0; i < _squadData.formations[_selectedFormation].width; i++)
    //     {
    //         for (int j = 0; j < _squadData.formations[_selectedFormation].depth; j++)
    //         {
    //             Vector3 diff = new Vector3((i * _squadData.formations[_selectedFormation].xSpacing) - ((_squadData.formations[_selectedFormation].width - 1) * _squadData.formations[_selectedFormation].xSpacing / 2f), 0f, ((j * _squadData.formations[_selectedFormation].zSpacing) - ((_squadData.formations[_selectedFormation].depth - 1) * _squadData.formations[_selectedFormation].zSpacing / 2f)) * -1f);
    //             Vector3 unitPos = (squadCenter + diff);
    //             GameObject wp = GameObject.Instantiate(_wayPointPrefab, this.transform);
    //             GameObject u = GameObject.Instantiate(_unitPrefab, unitPos, position.rotation);
    //             wp.name = "fw_" + _squadId + "_" + i + "x" + j;
    //             wp.transform.position = unitPos;
    //             u.name = "unit-" + _squadId + "_" + i + "x" + j;
    //             //u.transform.position = unitPos;
    //             Pawn newUnit = u.GetComponent<Pawn>();
    //             u.tag = _customTag;
    //             // newUnit.SetupUnit(_squadData, false, wp.transform, _squadId, _teamId, _stance);
    //             squadUnits[i, j] = newUnit;
    //         }
    //     }
    // }

    // public void MoveSquadCoroutine()
    // {
    //     StartCoroutine(PerformSquadMovement());
    // }

    // IEnumerator PerformSquadMovement()
    // {
    //     for (int i = 0; i < _squadData.formations[_selectedFormation].depth; i++)
    //     {
    //         for (int j = 0; j < _squadData.formations[_selectedFormation].width; j++)
    //         {
    //             // squadUnits[j, i].MoveUnit();
    //         }
    //         yield return new WaitForSeconds(_moveDelay);
    //     }
    // }

    // public void AttackAnotherSquad(Squad target)
    // {
    //     StartCoroutine(PerformSquadAttack(target.SquadId));
    // }

    // public void AISetSquadInCombatMode()
    // {
    //     for (int i = 0; i < _squadData.formations[_selectedFormation].width; i++)
    //     {
    //         for (int j = 0; j < _squadData.formations[_selectedFormation].depth; j++)
    //         {
    //             if (squadUnits[i, j] != null)
    //             {
    //                 // if (!squadUnits[i, j].currentState.Equals(Pawn.UnitState.death))
    //                 //     squadUnits[i, j].currentState = Pawn.UnitState.combat;
    //             }
    //         }
    //     }
    // }

    // IEnumerator PerformSquadAttack(int s)
    // {
    //     for (int i = 0; i < _squadData.formations[_selectedFormation].depth; i++)
    //     {
    //         for (int j = 0; j < _squadData.formations[_selectedFormation].width; j++)
    //         {
    //             // squadUnits[j, i].AttackSquad(s);
    //         }
    //         yield return new WaitForSeconds(0.1f);
    //     }
    // }

    // public void CalculateCenterOfSquad()
    // {
    //     int count = 0;
    //     Vector3 addedPositions = Vector3.zero;
    //     for (int i = 0; i < _squadData.formations[_selectedFormation].width; i++)
    //     {
    //         for (int j = 0; j < _squadData.formations[_selectedFormation].depth; j++)
    //         {
    //             if (squadUnits[i, j] != null)
    //             {
    //                 addedPositions += squadUnits[i, j].transform.position;
    //                 count++;
    //             }

    //         }
    //     }
    //     if (count == 0)
    //     {
    //         //Destroy Banner and squad
    //         if (_banner != null)
    //             Destroy(_banner.gameObject);
    //     }
    //     else
    //     {
    //         _banner.SetPosition(addedPositions / count);
    //     }
    // }

    // /// <summary>
    // /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    // /// </summary>
    // void FixedUpdate()
    // {
    //     CalculateCenterOfSquad();
    // }
}
