﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Core : MonoBehaviour
{
    public static Core instance;
    private PlayerDetails playerDetails;
    private List<Deck> decks;

    public PlayerDetails Player
    {
        get { return playerDetails; }
        set { playerDetails = value; }
    }

    public List<Deck> Decks
    {
        get { return decks; }
        set { decks = value; }
    }

    void Start()
    {
        if (instance == null)
            instance = this;
    }
}

