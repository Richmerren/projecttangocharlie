﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCharacterController : MonoBehaviour
{
    [SerializeField] private float actualSpeed = 3.5f;
    [SerializeField] private float rotationSpeed = 1f;
    [SerializeField] private Animator playerAnim;

    private float attackCharge = 0f;
    private bool isCharging = false;
    private bool isAttacking = false;


    void PlayerMovement()
    {
        if (!ThirdPersonCameraController.instance.IsRTS)
        {
            float hor = Input.GetAxis("Horizontal");
            float ver = Input.GetAxis("Vertical");
            Vector3 playerMovement = new Vector3(0f, 0f, ver) * actualSpeed * Time.deltaTime;

            Vector3 playerRotation = new Vector3(0f, hor, 0f) * rotationSpeed;

            transform.Translate(playerMovement, Space.Self);
            transform.Rotate(playerRotation, Space.Self);

            playerAnim.SetFloat("Speed", ver * actualSpeed);
        }
    }

    void PlayerActions()
    {
        if (!ThirdPersonCameraController.instance.IsRTS)
        {
            if (Input.GetMouseButtonDown(0))
            {
                isCharging = true;
                playerAnim.SetBool("attackCharge", true);
            }
            if (isCharging)
            {
                attackCharge += Time.deltaTime;
            }
            if (Input.GetMouseButtonUp(0))
            {
                if (attackCharge > Time.deltaTime)
                {
                    playerAnim.SetTrigger("doAttack");
                    playerAnim.SetBool("attackCharge", false);
                }
                else
                {
                    playerAnim.SetTrigger("doLowAttack");
                }
                isCharging = false;
                attackCharge = 0f;
                playerAnim.SetBool("attackCharge", false);

            }

            if (Input.GetMouseButtonDown(1))
            {
                playerAnim.SetBool("isBlocking", true);
            }
            if (Input.GetMouseButtonUp(1))
            {
                playerAnim.SetBool("isBlocking", false);
            }
        }
    }

    private void Update()
    {
        PlayerMovement();
        PlayerActions();
    }


}
