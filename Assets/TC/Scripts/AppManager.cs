﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : MonoBehaviour
{
    public static AppManager instance;
    [SerializeField] private AppState currentState;
    public TextAsset playerUnitList;
    public TextAsset enemyUnitList;

    // [SerializeField] private 
    public AppState CurrentState
    {
        get { return currentState; }
        set { currentState = value; }
    }
    // Start is called before the first frame update
    void Start()
    {
        // Application.targetFrameRate = 30;
        if (instance == null)
            instance = this;


    }

    // Update is called once per frame
    void Update()
    {

    }

    public enum AppState
    {
        Main,
        ArmySelect,
        BattlePlans,
        Battle,
        PauseScreen
    }
}
