﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnAnimation : MonoBehaviour
{
    [SerializeField] private Animator anim;

    private void Update()
    {

    }

    public void Idle()
    {
        anim.SetBool("isMoving", false);
        anim.SetBool("inCombat", false);
    }

    public void Run()
    {
        anim.SetBool("isMoving", true);
        anim.SetBool("isCharging", false);
    }
}
