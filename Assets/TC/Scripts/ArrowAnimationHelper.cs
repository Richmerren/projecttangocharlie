﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowAnimationHelper : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject arrow;
    public void DrawArrowGO()
    {
        arrow.SetActive(true);
    }
    public void LooseArrowGO()
    {
        arrow.SetActive(false);

    }
}
