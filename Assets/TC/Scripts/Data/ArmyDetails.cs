﻿using System.Collections;
using System.Collections.Generic;


public class ArmyDetails
{
    public int TeamId;
    public string ArmyId;
    public string ArmyName;
    public string CommanderId;
    public string CommanderName;
    public List<UnitDetails> UnitList;
}
