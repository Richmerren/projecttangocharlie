﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;


public class BattleCard : MonoBehaviour
{
    [SerializeField] private string _troopId;
    [SerializeField] private GameObject _selected;

    // Start is called before the first frame update
    void Start()
    {
        BattleManager.instance.TroopCardSelected += CardSelectionEvent;

    }

    public void SetTroopId(string id)
    {
        _troopId = id;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void CardSelectionEvent(string id)
    {
        if (_troopId != id)
        {
            _selected.SetActive(false);
        }
    }

    public void isSelected()
    {
        _selected.SetActive(true);
        BattleManager.instance.SelectTroop(_troopId);
    }
}
