﻿using UnityEngine;

public class UnitAnimation : MonoBehaviour
{
    [SerializeField] private Animator anim;

    private void Update()
    {

    }

    public void SetAnimationController(RuntimeAnimatorController r)
    {
        anim.runtimeAnimatorController = r;
        anim.Play("idle");
    }

    public void Idle()
    {
        anim.SetBool("isMoving", false);
        anim.SetBool("inCombat", false);
    }

    public void March(){
        anim.SetBool("isMoving", true);
        anim.SetBool("isCharging", false);
    }


    // public void March(){
    //     //anim.Play("infantry_spear_walk_march");
    //     anim.SetBool("run",false);
    //     anim.SetBool("combat",false);
    //     anim.SetBool("idle",false);
    //     anim.SetBool("march",true);
    // }

    // public void Run(){
    //     //anim.Play("infantry_spear_run");
    //     anim.SetBool("run",true);
    //     anim.SetBool("combat",false);
    //     anim.SetBool("idle",false);
    //     anim.SetBool("march",false);
    // }

    // public void Halt(){
    //     //anim.Play("infantry_spear_idle");
    //     anim.SetBool("run",false);
    //     anim.SetBool("combat",false);
    //     anim.SetBool("idle",true);
    //     anim.SetBool("march",false);
    // }

    // public void Brace(){
    //     //anim.Play("infantry_spear_block_idle");
    //     anim.SetBool("run",false);
    //     anim.SetBool("combat",true);
    //     anim.SetBool("idle",false);
    //     anim.SetBool("march",false);
    // }

    // public void ChargeAttack(){
    //     //anim.Play("infantry_spear_run_charge");
    //     anim.SetTrigger("charge");
    // }

    // public void TakeHit(){
    //     anim.SetTrigger("takeHit");
    // }

    // public void BlockCharge() {
    //     //anim.Play("infantry_spear_block_impact2");
    //     //anim.SetTrigger("brace");
    //     anim.SetTrigger("takeHit");
    // }

    // public void InCombat(){
    //     //anim.Play("infantry_spear_block_idle");
    //     anim.SetBool("run",false);
    //     anim.SetBool("combat",true);
    //     anim.SetBool("idle",false);
    //     anim.SetBool("march",false);
    // }

    // public void Death1(){
    //     //anim.Play("infantry_spear_death1");
    //     anim.SetBool("death",true);
    // }

    // public void Attack(){
    //     //anim.Play("infantry_spear_thrust");
    //     //anim.SetTrigger("combat");
    //     anim.SetTrigger("attack");
    // }

}
