﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointController : MonoBehaviour
{

    [SerializeField]
    private Renderer render;
    private float timer;

    public void PlaceWayPointForMovement(Vector3 pos, bool attack = false)
    {
        if (!attack)
        {
            this.transform.position = pos;
			render.enabled = true;
        }else{
			this.transform.position = pos;
		}
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        if(render.enabled){
			if(timer <= 0f){
				render.enabled = false;
			}else{
				timer -= Time.deltaTime;
			}
		}

    }
}
