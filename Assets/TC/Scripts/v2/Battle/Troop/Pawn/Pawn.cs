﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour
{
    [SerializeField] private PawnMovement movementController;
    [SerializeField] private PawnAnimation animationsController;
    [SerializeField] private Transform formationPosition;
    [SerializeField] private PawnState pawnState;
    [SerializeField] private PawnStance pawnStance;
    [SerializeField] private int teamId;
    [Header("Customization Objects")]
    [SerializeField] private Transform helmetObj;
    [SerializeField] private Transform primaryWeapon;
    [SerializeField] private Transform offHand;
    [SerializeField] private SkinnedMeshRenderer meshRenderer;
    public string troopId;
    private bool followingEnemyInCombat = false;
    private bool canMove = false;
    private int hp;
    private int attack; //How many attacks per minute
    private int defense; //Chance of blocking attack
    private int charge; //Charge Damage
    private int speed; //Movement speed
    private int morale; //Morale meter
    private int stamina; //Depletes every time you run/attack/defend
    private int range; //Range of visibility

    private float attackCooldown = 1.5f;
    private bool isCommander = false;
    private string preferredName = "";

    public enum PawnStance
    {
        aggressive,
        defensive,
        patrol,
        neutral
    }

    public enum PawnState
    {
        idle,
        combat,
        route,
        march,
        run,
        death
    };


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CalculateStopDistanceInBattle();
    }
    #region PROPERTIES

    public PawnState currentState
    {
        get { return pawnState; }
        set { pawnState = value; }
    }

    public PawnStance currentStance
    {
        get { return pawnStance; }
        set { currentStance = value; }
    }

    public int TeamId
    {
        get { return teamId; }
        set { teamId = value; }
    }
    #endregion //PROPERTIES
    public void SetupPawn(UnitDetails ud, bool showcase = false, Transform formationPos = null, string troop = "0", int team = 0, PawnStance st = PawnStance.neutral)
    {
        preferredName = ud.preferredName;
        if (!showcase)
        {
            formationPosition = formationPos;
            movementController.Warp(formationPos.position);
            troopId = troop;
            teamId = team;
            pawnState = PawnState.idle;
            pawnStance = st;
            attack = ud.attack;
            defense = ud.defense;
            charge = ud.charge;
            speed = ud.speed;
            morale = ud.morale;
            range = ud.range;
        }
        isCommander = ud.isCommander;
        SetupWeaponsAndArmor(ud, formationPosition.GetComponent<MeshRenderer>());
    }

    public void SetupWeaponsAndArmor(UnitDetails ud, MeshRenderer formationArrow)
    {
        /*
        Step 0: get color from troop
        Step 1: Choose Helmet
        Step 2: Choose Main weapon
        Step 3: Choose Main shield
        */

        // Appearance armorSettings = ud.appearance.Find(x => x.name.Equals("armor"));
        // Appearance colorSettings = ud.appearance.Find(x => x.name.Equals("tunic"));

        
    }

    public void MovePawn()
    {
        movementController.WarpAndMove(formationPosition.position);
        pawnState = PawnState.march;
        animationsController.Run();
    }

    public void PlacePawn()
    {
        movementController.Warp(formationPosition.position);
    }

    public void OnUnitStopped()
    {
        pawnState = PawnState.idle;
        this.transform.rotation = formationPosition.rotation;
        // anims.Halt();
    }

    void CalculateStopDistanceInBattle()
    {
        if (AppManager.instance.CurrentState == AppManager.AppState.Battle)
            if (movementController.IsMoving)
            {
                if (!movementController.Agent.pathPending)
                {
                    if (movementController.Agent.remainingDistance <= movementController.Agent.stoppingDistance)
                    {
                        if (!movementController.Agent.hasPath || movementController.Agent.velocity.sqrMagnitude == 0f)
                        {
                            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, formationPosition.rotation, 15f);
                            movementController.StopAgent();
                            animationsController.Idle();
                        }
                    }
                }
            }
    }
}
