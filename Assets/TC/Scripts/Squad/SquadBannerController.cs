﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquadBannerController : MonoBehaviour {
	// Use this for initialization
	[SerializeField]
	private Transform camera;
	[SerializeField]
	private GameObject selectedArrow;
	public string squadId;
	void Start () {
		camera  = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {
		// if(BattleFieldCommander.instance.squadSelected){
		// 	if(BattleFieldCommander.instance.selected._id == squadId){
		// 		selectedArrow.SetActive(true);
		// 	}else{
		// 		selectedArrow.SetActive(false);
		// 	}
		// }
	}

	public void SetPosition(Vector3 pos){
		this.transform.position = pos;
		Vector3 cameraToLow = new Vector3(camera.position.x,transform.position.y,camera.position.z);
		transform.LookAt(cameraToLow, transform.up);
	}
}
