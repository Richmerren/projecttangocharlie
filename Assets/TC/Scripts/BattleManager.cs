﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BattleManager : MonoBehaviour
{
    public static BattleManager instance;

    public event Action<string> TroopCardSelected;
    public Camera _gameCamera;
    [SerializeField] private List<UnitDetails> playerUnitDetails;
    // [SerializeField] private List<Troop> playerUnits;
    [SerializeField] private Dictionary<string, Troop> playerTroops;
    [SerializeField] private GameObject _troopPrefab;
    [SerializeField] private GameObject _troopBannerPrefab;
    [SerializeField] private string _selectedTroop;
    [SerializeField] private GameObject[] _cardPrefabs;
    [SerializeField] private Transform _cardParent;
    [SerializeField] private GameObject _MenuButton;
    [Header("Battle Plans")]
    // [SerializeField] private GameObject _troopMenuPanel;

    [Header("Battle Plans")]
    [SerializeField] private GameObject _battleUIGO;

    [Header("Pause Menu")]
    [SerializeField] private GameObject _pauseMenu;
    // [Header("Materials Copy")]
    // [SerializeField] private Material _skinMaterial;
    // [SerializeField] private Material _tunicMaterial;
    // [SerializeField] private Material _armorMaterial;

    public string Troop
    {
        get { return _selectedTroop; }
    }
    // public List<Troop> Units
    // {
    //     get { return playerUnits; }
    // }
    // Use this for initialization
    void Start()
    {
        // playerUnits = new List<Troop>();
        playerTroops = new Dictionary<string, Troop>();
        if (instance == null)
            instance = this;
        if (AppManager.instance != null)
        {
            ArmyDetails ad = JsonUtility.FromJson<ArmyDetails>(AppManager.instance.playerUnitList.text);
            playerUnitDetails = ad.UnitList;
            foreach (UnitDetails ud in playerUnitDetails)
            {
                int cardPos = 0;
                if (ud.unitClass.Equals("InfantrySpear"))
                {
                    cardPos = 0;
                }
                if (ud.unitClass.Equals("InfantrySword"))
                {
                    cardPos = 1;
                }
                GameObject troopCard = GameObject.Instantiate(_cardPrefabs[cardPos], _cardParent);
                BattleCard card = troopCard.GetComponent<BattleCard>();
                GameObject troop = GameObject.Instantiate(_troopPrefab);
                troop.name = ud._id;
                GameObject unit = Instantiate(Resources.Load(ud.isCommander ? ud.heroPrefabPath : ud.unitPrefabPath, typeof(GameObject))) as GameObject;
                Troop t = troop.GetComponent<Troop>();
                card.SetTroopId(ud._id);
                t.CreateTroop(ud, true, unit, ud._id);
                playerTroops.Add(ud._id, t);
                GameObject troopBanner = GameObject.Instantiate(_troopBannerPrefab);
                TroopBanner tb = troopBanner.GetComponent<TroopBanner>();
                tb.name = "banner_" + ud._id;
                t.SetBanner(tb);
                Destroy(unit);
            }
        }
        BattleInputController.instance.OnTroopClicked += SelectTroop;

        BattleInputController.instance.OnTroopSet += OrderTroop;
        BattleInputController.instance.OnTroopRotated += RotateTroop;
        BattleInputController.instance.OnTroopOrdered += MoveTroop;

        BattleInputController.instance.OnTroopDeselect += DeselectTroop;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartBattle()
    {
        _battleUIGO.SetActive(false);
        _MenuButton.SetActive(true);
        AppManager.instance.CurrentState = AppManager.AppState.Battle;
    }

    public void PauseBattle()
    {
        AppManager.instance.CurrentState = AppManager.AppState.PauseScreen;
        _pauseMenu.SetActive(true);
        _MenuButton.SetActive(false);
    }
    public void ResumeBattle()
    {
        AppManager.instance.CurrentState = AppManager.AppState.Battle;
        _pauseMenu.SetActive(false);
        _MenuButton.SetActive(true);
    }
    public void QuitBattle()
    {
        AppManager.instance.CurrentState = AppManager.AppState.Main;
    }

    public bool isTroopSelected = false;

    public Troop GetSelectedTroop()
    {
        if (!string.IsNullOrEmpty(_selectedTroop))
        {
            // Troop t = playerUnits.Find((x) => x._id == _selectedTroop);
            // return t;
            return playerTroops[_selectedTroop];
        }
        else
        {
            return null;
        }
    }

    private void DeselectTroop()
    {
        Troop tmp;
        playerTroops.TryGetValue(_selectedTroop, out tmp);
        if (tmp != null)
        {
            playerTroops[_selectedTroop].SelectTroop(false);
            if (TroopCardSelected != null)
                TroopCardSelected.Invoke(string.Empty);
            _selectedTroop = string.Empty;
            isTroopSelected = false;
            
        }
    }

    public void SelectTroop(string id)
    {
        if (TroopCardSelected != null)
        {
            TroopCardSelected.Invoke(id);
        }
        if (!string.IsNullOrEmpty(_selectedTroop))
        {
            playerTroops[_selectedTroop].SelectTroop(false);
        }
        _selectedTroop = id;
        isTroopSelected = true;
        
        playerTroops[id].SelectTroop(true);
    }


    public void OrderTroop(Vector3 position)
    {
        playerTroops[_selectedTroop].SetWaypoint(position);
    }

    public void RotateTroop(Vector3 rotation)
    {
        playerTroops[_selectedTroop].RotateWaypoint(rotation);
    }
    public void MoveTroop()
    {
        playerTroops[_selectedTroop].MoveTroopCoroutine();
    }
}
