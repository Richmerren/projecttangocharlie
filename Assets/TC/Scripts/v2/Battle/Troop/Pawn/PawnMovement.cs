﻿using UnityEngine;
using UnityEngine.AI;
using System;
public class PawnMovement : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent;

    public Vector3 destination;
    private bool isMoving = false;

    public NavMeshAgent Agent
    {
        get { return agent; }
    }

    public bool IsMoving
    {
        get { return isMoving; }
        set { isMoving = value; }
    }

    private void Start()
    {

    }

    public void Warp(Vector3 position)
    {
        agent.Warp(position);
    }

    private void Update()
    {

    }
    public void WarpAndMove(Vector3 destination)
    {
        agent.Warp(this.transform.position);
        agent.speed = 2f;
        agent.stoppingDistance = 0f;
        this.destination = destination;
        agent.SetDestination(destination);
        isMoving = true;
    }

    public void MoveAgent(Vector3 destination)
    {
        agent.speed = 2f;
        agent.stoppingDistance = 0f;
        this.destination = destination;
        agent.SetDestination(destination);
        isMoving = true;
    }

    public void StopAgent()
    {
        agent.Warp(destination);
        isMoving = false;
        agent.isStopped = true;
    }


}
