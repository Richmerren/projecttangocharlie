﻿using UnityEngine;
using UnityEngine.AI;
using System;
public class UnitMovement : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent;
    
    public Vector3 destination;
    private bool isMoving = false;

    public NavMeshAgent Agent
    {
        get { return agent; }
    }

    public bool IsMoving
    {
        get { return isMoving; }
        set { isMoving = value; }
    }

    private void Start()
    {

    }

    public void Warp(Vector3 position)
    {
        agent.Warp(position);
    }

    private void Update()
    {
        
    }

    public void ChargeAgent(Vector3 destination)
    {
        if (!agent.isOnNavMesh)
        {
            if (agent.Warp(transform.position))
            {
                WarpAndMove(destination);
            }
        }
        else
        {
            WarpAndMove(destination);
        }
    }

    private void WarpAndMove(Vector3 destination)
    {
        agent.speed = 5f;
        agent.stoppingDistance = 1f;
        this.destination = destination;
        agent.SetDestination(destination);
        isMoving = true;
    }

    public void MoveAgent(Vector3 destination)
    {
        agent.speed = 2f;
        agent.stoppingDistance = 0f;
        this.destination = destination;
        agent.SetDestination(destination);
        isMoving = true;
    }

    public void ChargeEnemy(GameObject enemyUnit)
    {
        Debug.Log("Chargeeeeee");
        agent.speed = 5f;
        //agent.isStopped = false;
        // if (unitBrain.LockedEnemy != null)
        // {
        //     destination = unitBrain.LockedEnemy.transform.position;
        //     agent.SetDestination(destination);
        //     move = true;
        // }
        // else
        // {

        // }

    }

    public void StopAgent()
    {
        isMoving = false;
        Debug.Log("stopping agent");
        agent.isStopped = true;
    }

    // public void StopAndFight()
    // {
    //     isMoving = false;
    //     agent.SetDestination(unitBrain.transform.position);
    // }


    public void BraceForImpact(Vector3 enemyPosition)
    {
        this.transform.LookAt(enemyPosition);
    }
}
