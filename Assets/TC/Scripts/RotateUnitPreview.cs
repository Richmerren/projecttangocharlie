﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class RotateUnitPreview : MonoBehaviour, IDragHandler, IEndDragHandler
{
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private float step;
    public float rotationXAxis = 90f;

    private bool dragging = false;
    private Vector3 initialTouch;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (dragging)
        {
            Vector3 newRotation = new Vector3(rotationXAxis, 90f, 90f);
            _spawnPoint.DOLocalRotate(newRotation, 0f);
        }
    }


    public void OnDrag(PointerEventData data)
    {
        dragging = true;
        rotationXAxis += data.delta.x;
    }

    public void OnEndDrag(PointerEventData data)
    {
        dragging = false;
    }

}
