﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCameraController : MonoBehaviour
{
    public static ThirdPersonCameraController instance;
    [SerializeField] private Transform soldier;
    [SerializeField] private float rotationSpeed = 1f;
    [SerializeField] private float cameraSpeed = 5f;
    [SerializeField] private float heightSpeed = 5f;
    [SerializeField] private float smoothSpeed = 0.125f;
    [SerializeField] private Transform target, _camera, _rts;
    private Vector3 initialCameraPosition;
    float zoomSpeed = 2f;
    float mouseX, mouseY;

    [SerializeField] private bool rtsOn = false;

    public bool IsRTS
    {
        get { return rtsOn; }
    }


    private void Start()
    {
        if (instance == null)
            instance = this;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        initialCameraPosition = _camera.localPosition;
    }

    void Update()
    {
        TabKeyChecker();
        HeightKeyChecker();
    }

    private void LateUpdate()
    {
        CameraRotationControl();
        PositionControl();
    }

    void PositionControl()
    {
        if (rtsOn)
        {
            float hor = Input.GetAxis("Horizontal");
            float ver = Input.GetAxis("Vertical");
            hor = Mathf.Clamp(hor * cameraSpeed * Time.deltaTime, -100f, 100f);
            ver = Mathf.Clamp(ver * cameraSpeed * Time.deltaTime, -100f, 100f);
            if (transform.position.x + hor > 100f || transform.position.x + hor < -100f)
            {
                hor = 0f;
            }
            if (transform.position.z + ver > 100f || transform.position.z + ver < -100f)
            {
                ver = 0f;
            }
            Vector3 cameraMovement = new Vector3(hor, 0f, ver);
            transform.Translate(cameraMovement, Space.Self);
        }
        else
        {
            this.transform.position = Vector3.Lerp(this.transform.position, soldier.position, smoothSpeed);
        }
    }

    void TabKeyChecker()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleCursor();
        }
    }

    void HeightKeyChecker()
    {
        if (rtsOn)
        {
            if (Input.GetKey(KeyCode.X))//UP
            {
                float h = Mathf.Clamp(transform.position.y + heightSpeed * Time.deltaTime, 1f, 20f);
                Vector3 height = new Vector3(transform.position.x, h, transform.position.z);
                transform.position = Vector3.Lerp(transform.position, height, 1f);
            }
            if (Input.GetKey(KeyCode.Z))//DOWN
            {
                float h = Mathf.Clamp(transform.position.y - heightSpeed * Time.deltaTime, 1f, 20f);
                Vector3 height = new Vector3(transform.position.x, h, transform.position.z);
                transform.position = Vector3.Lerp(transform.position, height, 1f);
            }

            if (Input.GetKey(KeyCode.Q))//TURN LEFT
            {
                float amount = transform.rotation.eulerAngles.y + rotationSpeed * Time.deltaTime;
                this.transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, amount, 0f), smoothSpeed);
            }
            if (Input.GetKey(KeyCode.E))//TURN RIGHT
            {
                float amount = transform.rotation.eulerAngles.y - rotationSpeed * Time.deltaTime;
                this.transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, amount, 0f), smoothSpeed);
            }
        }
    }

    void CameraRotationControl()
    {
        mouseX += Input.GetAxis("Mouse X");
        mouseY -= Input.GetAxis("Mouse Y");
        mouseY = Mathf.Clamp(mouseY, -25f, 60f);
        if (rtsOn)
        {
            ZoomOut();
            // this.transform.rotation = Quaternion.Lerp(target.rotation, Quaternion.Euler(0f, mouseX, 0f), smoothSpeed);
        }
        else
        {
            ZoomIn();
            _camera.LookAt(target);
            this.transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(mouseY, mouseX, 0f), smoothSpeed);
        }
    }

    void ZoomOut()
    {
        _camera.transform.localPosition = Vector3.Lerp(_camera.localPosition, _rts.localPosition, 1f);
        _camera.transform.localRotation = Quaternion.Lerp(_camera.rotation, Quaternion.Euler(new Vector3(45f, 0f, 0f)), 1f);
        this.transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f), smoothSpeed);
    }

    void ZoomIn()
    {
        _camera.transform.localPosition = Vector3.Lerp(_camera.localPosition, initialCameraPosition, 1f);
    }

    void ToggleCursor()
    {
        if (rtsOn)
        {
            rtsOn = false;
            Cursor.visible = rtsOn;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            rtsOn = true;
            Cursor.visible = rtsOn;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    // void RayCastToGround()
    // {
    //     Debug.DrawRay(_camera.position, Vector3.down * 100f, Color.red, Time.deltaTime);
    //     RaycastHit hit;
    //     if (Physics.Raycast(_camera.position, Vector3.down, out hit))
    //     {
    //         Debug.Log("HIT: " + hit.collider.tag);
    //         if (hit.collider.tag.Equals("Field"))
    //         {
    //             transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, hit.point.y, transform.position.z), 1f);
    //         }
    //     }
    // }
}
