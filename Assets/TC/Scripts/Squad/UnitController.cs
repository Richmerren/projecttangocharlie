﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnitController : MonoBehaviour
{
    public enum Culture
    {
        hellenic,
        barbarian,
        saxon,
        egyptian,
        african,
        chinese,
        japanese,
        mongol,
        roman
    }
    public enum Category
    {
        lightInfantry,
        mediumInfantry,
        heavyInfantry,
        ranged,
        cavalry,
        siege,
        special
    }
    public Culture culture;
    public Category category;

    [SerializeField]
    private GameObject head;
    [SerializeField]
    private GameObject shield;
    [SerializeField]
    private GameObject bowStaff;
    [SerializeField]
    private GameObject body;
    [SerializeField]
    private GameObject primaryWeapon;

}
