﻿using System;
using UnityEngine;

public class BattleInputController : MonoBehaviour
{
    public static BattleInputController instance;
    public event Action<Vector3> OnTroopSet;
    public event Action<Vector3> OnTroopRotated;
    public event Action OnTroopOrdered;
    public event Action<string> OnTroopClicked;
    public event Action OnTroopDeselect;
    public event Action<string> OnSquadAttacked;
    private bool isDragging;
    private float timeDragged = 0f;
    private Vector3 startingDragPoint;
    private Vector3 dragDirection;
    public Camera cam;
    // Use this for initialization
    void Start()
    {
        if (instance == null)
            instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        BattlePlansInput();
        BattleSceneInput();
    }

    void BattlePlansInput()
    {
        if (AppManager.instance.CurrentState == AppManager.AppState.BattlePlans)
        {
            if (Input.GetMouseButtonDown(1))
            {
                if (!String.IsNullOrEmpty(BattleManager.instance.Troop))
                {
                    SetTroopInBattleField();
                }
            }
        }
    }

    void BattleSceneInput()
    {
        if (AppManager.instance.CurrentState == AppManager.AppState.Battle)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    // if (hit.collider.tag.Equals("PlayerUnits") || hit.collider.tag.Equals("EnemyUnits"))
                    if (hit.collider.tag.Equals("PlayerUnits"))
                    {
                        Pawn u = hit.transform.gameObject.GetComponent<Pawn>();
                        if (OnTroopClicked != null && u != null)
                            OnTroopClicked(u.troopId);
                    }
                    else
                    {
                        if (OnTroopDeselect != null)
                            OnTroopDeselect();
                    }
                }
            }

            if (Input.GetMouseButtonDown(1))
            {
                if (BattleManager.instance.isTroopSelected)
                {
                    if (!isDragging)
                    {
                        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                        RaycastHit hit;
                        if (Physics.Raycast(ray, out hit))
                        {
                            if (hit.collider.tag.Equals("Field"))
                            {
                                startingDragPoint = hit.point;
                                timeDragged = 0f;
                                isDragging = true;
                                if (OnTroopSet != null)
                                    OnTroopSet(hit.point);
                            }
                            // if (hit.collider.gameObject.tag.Equals("PlayerUnits") || hit.collider.tag.Equals("EnemyUnits"))
                            // {
                            //     Pawn clicked = hit.collider.gameObject.GetComponent<Pawn>();
                            //     if (clicked != null)
                            //         if (clicked.TeamId != BattleFieldCommander.instance.teamId)
                            //         {
                            //             OnSquadAttacked(clicked.troopId);
                            //         }
                            // }
                        }
                    }

                }
            }
            if (Input.GetMouseButton(1))
            {
                timeDragged += Time.deltaTime;
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.tag.Equals("Field"))
                    {
                        if (OnTroopRotated != null)
                            OnTroopRotated(hit.point);
                    }
                }
            }

            if (Input.GetMouseButtonUp(1))
            {
                isDragging = false;
                if (OnTroopOrdered != null)
                    OnTroopOrdered();
            }
        }

    }

    void SetTroopInBattleField()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag.Equals("Field"))
            {
                Troop t = BattleManager.instance.GetSelectedTroop();
                if (t != null)
                {
                    t.SetTroop(hit.point);
                }
            }
        }
    }
}
