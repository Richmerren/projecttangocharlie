﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Unit Card", menuName = "New Unit Card")]
public class UnitCard : ScriptableObject
{
    public string id;
    public new string name;
    public string description;
    public string preferredName;
    public string unitClass;
    public int size;
    public int hp;
    public int attack;
    public int defense;
    public int charge;
    public int speed;
    public int morale;
    public int stamina;
    public int range;
    public float experience;
    public int weaponAndArmor;
    public int tier;
    public float skinToneOffsetY;
    public List<Formation> formations;
    public PerformanceStats performanceStats;

    public void Setup(UnitDetails ud)
    {
        name = ud.name;
        description = ud.description;
        preferredName = ud.preferredName;
        unitClass = ud.unitClass.ToString();
        size = ud.width*ud.depth;
        hp = ud.hp;
        attack = ud.attack;
        defense = ud.defense;
        charge = ud.charge;
        speed = ud.charge;
        morale = ud.morale;
        stamina = ud.stamina;
        range = ud.range;
        experience = ud.experience;
        weaponAndArmor = ud.weaponAndArmor;
        tier = ud.tier;
        skinToneOffsetY = skinToneOffsetY;
        formations = ud.formations;
    }
}
