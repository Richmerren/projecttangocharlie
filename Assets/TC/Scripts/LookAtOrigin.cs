﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtOrigin : MonoBehaviour {
    public GameObject _camera;
    private Vector3 origin;
	// Use this for initialization
	void Start () {
        origin = new Vector3(0f, 0f, 0f);
	}
	
	// Update is called once per frame
	void Update () {
        _camera.transform.LookAt(origin, Vector3.up);
	}
}
