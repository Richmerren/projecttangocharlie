﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelperFiles : MonoBehaviour
{
    public static HelperFiles instance;
    // [SerializeField] private List<Animator> animators;
    [SerializeField] private List<Sprite> unitIcons;
    [SerializeField] private List<Texture> sigils;    

    // Start is called before the first frame update
    void Start()
    {

        if (instance == null)
            instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }
    // public Animator GetUnitAnimator(string _name)
    // {
    //     for (int i = 0; i < animators.Count; i++)
    //     {
    //         if (animators[i].gameObject.name.Equals(_name))
    //             return animators[i];
    //     }
    //     return null;
    // }

    public Texture GetSigilTexture(string name)
    {
        Texture t = sigils.Find(x => x.name.Equals(name));
        return t;
    }

    public string GetSigilName(int i)
    {
        return sigils[i].name;
    }

    public int SigilCount()
    {
        return sigils.Count;
    }
}
