﻿using System;

public class EquipmentData {
    public string name;
    public EquipmentType equipType;
    public float dmg;
    public float def;
    public int rating;
    public float colorOffsetX;
    public float colorOffsetY;
}

public enum EquipmentType {
    sword,
    spear,
    axe,
    pike,
    arrow,
    shield,
    throwable
}
