﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TroopBanner : MonoBehaviour
{
    // Use this for initialization
    [SerializeField]
    private Transform _camera;
    [SerializeField] private GameObject selectedArrow;
    [SerializeField] private MeshRenderer flagRenderer;
    [SerializeField] private MeshRenderer arrowRenderer;

    public string troopId;
    void Start()
    {

    }

    public void SetupBanner(UnitDetails ud)
    {
        _camera = Camera.main.transform;
        Appearance colorSettings = ud.appearance.Find(x => x.name.Equals("tunic"));
        arrowRenderer.sharedMaterial.SetTextureOffset("_MainTex", new Vector2(colorSettings.offsetX, colorSettings.offsetY));
        flagRenderer.sharedMaterials[0].SetTextureOffset("_MainTex", new Vector2(colorSettings.offsetX, colorSettings.offsetY));
        flagRenderer.sharedMaterials[1].SetTexture("_MainTex", HelperFiles.instance.GetSigilTexture(ud.sigil));
    }

    // Update is called once per frame
    void Update()
    {
        if (_camera != null)
            RotateBanner();
        if (AppManager.instance.CurrentState == AppManager.AppState.Battle)
            if (BattleManager.instance.isTroopSelected)
            {
                if (BattleManager.instance.Troop.Equals(troopId))
                {
                    selectedArrow.SetActive(true);
                }
                else
                {
                    selectedArrow.SetActive(false);
                }
            }
    }

    public void SetPosition(Vector3 pos)
    {
        this.transform.position = pos;
        RotateBanner();
    }

    public void RotateBanner()
    {
        Vector3 cameraToLow = new Vector3(_camera.position.x, transform.position.y, _camera.position.z);
        transform.LookAt(cameraToLow, transform.up);

    }

    public void SetSelected(bool value)
    {
        selectedArrow.SetActive(value);
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonUp(0))
        {
            BattleManager.instance.SelectTroop(troopId);
        }
    }
}
