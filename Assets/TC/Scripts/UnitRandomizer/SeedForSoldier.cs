﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Text = TMPro.TMP_Text;
using System.IO;


public class SeedForSoldier : MonoBehaviour
{
    [SerializeField] private List<UnitDetails> herosList;

    [SerializeField] private GameObject _randomizeButton;
    [SerializeField] private GameObject _spawnButton;
    [SerializeField] private GameObject _saveButton;
    [SerializeField] private TextAsset _selectionSeed;


    [SerializeField] private Transform _heroSpawnPoint;
    [SerializeField] private Text[] _heroNames;
    [SerializeField] private Transform _unit1SpawnPoint;

    [SerializeField] private Transform _unit2SpawnPoint;

    [SerializeField] private Transform _unit3SpawnPoint;

    [SerializeField] private GameObject[] spawnedUnits;

    private RandomizerData data;
    void Start()
    {
        spawnedUnits = new GameObject[4];

        herosList = new List<UnitDetails>();
        data = JsonUtility.FromJson<RandomizerData>(_selectionSeed.text);
    }

    public void ShowGameObjects()
    {
        foreach (GameObject g in spawnedUnits)
        {
            g.SetActive(true);
        }
    }

    public void SaveHeroDatabaseToFile()
    {
        string path = "Assets/Resources/heros1.json";

        HeroList h = new HeroList();
        h.heroList = herosList;
        StreamWriter writer = new StreamWriter(path, true);
        string list = JsonUtility.ToJson(h);
        writer.WriteLine(list);
        writer.Close();
    }

    // Update is called once per frame
    void Update()
    {
        _spawnButton.gameObject.SetActive(spawnedUnits[0] == null ? false : true);
    }

    public void RandomizeHero()
    {
        GenerateHero(_heroSpawnPoint, 3);
        GenerateHero(_unit1SpawnPoint, 0);
        GenerateHero(_unit2SpawnPoint, 1);
        GenerateHero(_unit3SpawnPoint, 2);
    }

    private void GenerateHero(Transform spawnPoint, int pos)
    {
        UnitDetails newHero = new UnitDetails();
        if (spawnedUnits[pos] != null)
        {
            Destroy(spawnedUnits[pos]);
        }
        int helmetPosition = Random.Range(0, data.helmets.Count);
        int objectPosition = Random.Range(0, data.helmets[helmetPosition].objects.Count);
        int plumePosition = data.helmets[helmetPosition].plumes.Count == 0 ? -1 : Random.Range(0, data.helmets[helmetPosition].plumes.Count);
        int additionalPosition = Random.Range(0, data.head.Count);
        int namePosition = Random.Range(0, data.names.Count);
        int locationPosition = Random.Range(0, data.locations.Count);
        int skinOffsetPosition = Random.Range(0, data.colorsAndFaces.skinOffsets.Count);
        int colorOffsetPosition = Random.Range(0, data.colorsAndFaces.otherOffsets.Count);
        int armorOffsetPosition = Random.Range(0, data.colorsAndFaces.armorOffsets.Count);
        int sigilPosition = Random.Range(0, HelperFiles.instance.SigilCount());
        int unitTypePosition = Random.Range(0, data.unitTypes.Count);
        int primeryHandPosition = Random.Range(0, data.unitTypes[unitTypePosition].primaryHand.Count);
        int offHandPosition = Random.Range(0, data.unitTypes[unitTypePosition].offHand.Count);


        newHero.helmetObj = data.helmets[helmetPosition].objects[objectPosition];
        newHero.additional = data.head[additionalPosition];
        newHero.plume = plumePosition == -1 ? "none" : data.helmets[helmetPosition].plumes[plumePosition];
        newHero.skinToneOffsetX = data.colorsAndFaces.skinOffsets[skinOffsetPosition];
        Appearance armorColor = new Appearance
        {
            name = "tunic",
            offsetX = data.colorsAndFaces.otherOffsets[colorOffsetPosition].x,
            offsetY = data.colorsAndFaces.otherOffsets[colorOffsetPosition].y
        };
        Appearance tunicColor = new Appearance
        {
            name = "armor",
            offsetX = data.colorsAndFaces.armorOffsets[armorOffsetPosition].x,
            offsetY = data.colorsAndFaces.armorOffsets[armorOffsetPosition].y
        };
        Appearance beards = new Appearance
        {
            name = "beards",
            offsetX = data.colorsAndFaces.otherOffsets[colorOffsetPosition].x,
            offsetY = data.colorsAndFaces.otherOffsets[colorOffsetPosition].y
        };
        Appearance faces = new Appearance
        {
            name = "faces",
            offsetX = data.colorsAndFaces.armorOffsets[armorOffsetPosition].x,
            offsetY = data.colorsAndFaces.armorOffsets[armorOffsetPosition].y
        };
        newHero.appearance = new List<Appearance>();
        newHero.appearance.Add(armorColor);
        newHero.appearance.Add(tunicColor);
        newHero.appearance.Add(beards);
        newHero.appearance.Add(faces);
        newHero.sigil = HelperFiles.instance.GetSigilName(sigilPosition);
        newHero.animator = data.animators[0];
        // newHero.animator = unitTypePosition == 1 ? data.animators[1] : data.animators[0];
        newHero.unitPrefabPath = data.prefabs[0];
        newHero.heroPrefabPath = data.prefabs[1];
        newHero.primaryWeapon = data.unitTypes[unitTypePosition].primaryHand[primeryHandPosition];
        newHero.offHand = data.unitTypes[unitTypePosition].offHand[offHandPosition];
        newHero.name = data.unitTypes[unitTypePosition].type;
        newHero.unitClass = data.unitTypes[unitTypePosition].type;
        newHero.commanderName = data.names[namePosition] + " of " + data.locations[locationPosition];
        newHero.isCommander = true;
        _heroNames[pos].text = newHero.commanderName;
        herosList.Add(newHero);
        SpawnSoldier(newHero, spawnPoint, pos);
    }
    private void GenerateSoldier(Transform spawnPoint, int pos)
    {
        UnitDetails newHero = new UnitDetails();
        if (spawnedUnits[pos] != null)
        {
            Destroy(spawnedUnits[pos]);
        }
        int helmetPosition = Random.Range(0, data.helmets.Count);
        int objectPosition = Random.Range(0, data.helmets[helmetPosition].objects.Count);
        // int namePosition = Random.Range(0, names.Length);
        int skinOffsetPosition = Random.Range(0, data.colorsAndFaces.skinOffsets.Count);
        int colorOffsetPosition = Random.Range(0, data.colorsAndFaces.otherOffsets.Count);
        int armorOffsetPosition = Random.Range(0, data.colorsAndFaces.armorOffsets.Count);
        int sigilPosition = Random.Range(0, HelperFiles.instance.SigilCount());
        int unitTypePosition = Random.Range(0, data.unitTypes.Count);
        int primeryHandPosition = Random.Range(0, data.unitTypes[unitTypePosition].primaryHand.Count);
        int offHandPosition = Random.Range(0, data.unitTypes[unitTypePosition].offHand.Count);


        newHero.helmetObj = data.helmets[helmetPosition].objects[objectPosition];
        newHero.additional = "none";
        newHero.plume = "none";
        newHero.skinToneOffsetX = data.colorsAndFaces.skinOffsets[skinOffsetPosition];
        Appearance armorColor = new Appearance
        {
            name = "tunic",
            offsetX = data.colorsAndFaces.otherOffsets[colorOffsetPosition].x,
            offsetY = data.colorsAndFaces.otherOffsets[colorOffsetPosition].y
        };
        Appearance tunicColor = new Appearance
        {
            name = "armor",
            offsetX = data.colorsAndFaces.armorOffsets[armorOffsetPosition].x,
            offsetY = data.colorsAndFaces.armorOffsets[armorOffsetPosition].y
        };
        Appearance beards = new Appearance
        {
            name = "beards",
            offsetX = data.colorsAndFaces.otherOffsets[colorOffsetPosition].x,
            offsetY = data.colorsAndFaces.otherOffsets[colorOffsetPosition].y
        };
        Appearance faces = new Appearance
        {
            name = "faces",
            offsetX = data.colorsAndFaces.armorOffsets[armorOffsetPosition].x,
            offsetY = data.colorsAndFaces.armorOffsets[armorOffsetPosition].y
        };
        newHero.appearance = new List<Appearance>();
        newHero.appearance.Add(armorColor);
        newHero.appearance.Add(tunicColor);
        newHero.appearance.Add(beards);
        newHero.appearance.Add(faces);
        newHero.sigil = HelperFiles.instance.GetSigilName(sigilPosition);
        newHero.animator = data.animators[0];
        // newHero.animator = unitTypePosition == 1 ? data.animators[1] : data.animators[0];
        newHero.unitPrefabPath = data.prefabs[0];
        newHero.heroPrefabPath = data.prefabs[1];
        newHero.primaryWeapon = data.unitTypes[unitTypePosition].primaryHand[primeryHandPosition];
        newHero.offHand = data.unitTypes[unitTypePosition].offHand[offHandPosition];
        newHero.name = data.unitTypes[unitTypePosition].type;
        newHero.unitClass = data.unitTypes[unitTypePosition].type;
        newHero.commanderName = "none";
        newHero.isCommander = false;
        SpawnSoldier(newHero, spawnPoint, pos);
    }

    private void SpawnSoldier(UnitDetails ud, Transform spawnPoint, int pos)
    {
        GameObject unit = Instantiate(Resources.Load(ud.isCommander ? ud.heroPrefabPath : ud.unitPrefabPath, typeof(GameObject)), spawnPoint) as GameObject;
        unit.name = ud.name;
        UnityEngine.AI.NavMeshAgent nma = unit.GetComponent<UnityEngine.AI.NavMeshAgent>();
        nma.enabled = false;
        // unit.AddComponent<RotateUnitPreview>();
        Pawn u = unit.GetComponent<Pawn>();
        u.SetupPawn(ud, true);

        spawnedUnits[pos] = unit;

        unit.SetActive(false);
    }
}
